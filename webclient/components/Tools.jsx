import React from 'react';
import Paper from 'material-ui/Paper';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TextField from 'material-ui/TextField';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import {CardActions, CardMedia, CardHeader, CardTitle, CardText} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import {Grid, Segment, Divider} from 'semantic-ui-react';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import Toggle from 'material-ui/Toggle';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import {
  Card,
  Icon,
  Popup,
  Reveal,
  Image,
  Label,
  Button
} from 'semantic-ui-react';
import {Step, Stepper, StepButton, StepLabel} from 'material-ui/Stepper';

const toggleStyle = {
  toggle: {
    marginTop: 0,
    marginLeft: 200
  }
};

const popStyle = {
  borderRadius: 0,
  opacity: 0.8,
  padding: '2em'
}

const styles = {
  margin: 14,
  backgroundColor: "#000000ba !important"
};

const style = {
  height: 250,
  width: '80%',
  margin: 'auto',
  textAlign: 'center',
  marginTop: '3%'
};

const cbStyle = {
  margin: 12
};

const btnStyle = {
  marginLeft: '0%',
  color: "black",
  borderStyle: "outset"
};

const muiTheme = getMuiTheme({});

const Logged = (props) => (<IconMenu {...props} iconButtonElement={<IconButton > <MoreVertIcon/></IconButton>} targetOrigin={{
    horizontal: 'right',
    vertical: 'top'
  }} anchorOrigin={{
    horizontal: 'right',
    vertical: 'top'
  }}>
  <MenuItem primaryText="Refresh"/>
  <MenuItem primaryText="Help"/>
  <MenuItem primaryText="Sign out"/>
</IconMenu>);

Logged.muiName = 'IconMenu';

const tilesData = [
  {
    name: 'gitlab',
    img: 'https://img.stackshare.io/service/880/lmalkclL.png',
    text: 'Version Control system. Connecting issue management, code review, CI, CD, and monitoring into a single, easy-to-install application.',
    alt: 'gitlab'
  }, {
    name: 'sonarqube',
    img: 'https://docs.sonarqube.org/download/attachments/6389860/SONARNEXT?version=1&modificationDate=1440424676000&api=v2',
    text: 'Version Control system. Connecting issue management, code review, CI, CD, and monitoring into a single, easy-to-install application.',
    alt: 'sonarqube'
  }, {
    name: 'jenkins',
    img: 'https://jenkins.io/images/226px-Jenkins_logo.svg.png',
    text: 'Continuous Integration and Continuous Delivery. Jenkins can be used as a simple CI server or turned into the continuous delivery hub.',
    alt: 'jenkins'
  }, {
    name: 'nagios',
    img: 'https://a.slack-edge.com/14d88/plugins/nagios/assets/service_512.png',
    text: 'A powerful monitoring system. It provides enterprise-class Source IT monitoring, network monitoring, server and applications monitoring.',
    alt: 'nagios'
  }, {
    name: 'selenium',
    img: 'https://avatars2.githubusercontent.com/u/983927?s=400&v=4',
    text: 'Selenium Grid and Automated Testing which is suitable for web applications across different browsers and different platforms.',
    alt: 'selenium'
  }, {
    name: 'gerrit',
    img: 'http://blogs.collab.net/wp-content/uploads/2016/02/diffy_inline_edit.png',
    text: 'Code Review for Git. Gerrit provides web based code review and repository management for the Git version control system.',
    alt: 'gerrit'
  }, {
    name: "elk",
    img: 'https://d1jnx9ba8s6j9r.cloudfront.net/blog/wp-content/uploads/2017/11/ELK.png',
    text: 'ELK is a combination of the massively popular Elasticsearch, Logstash, and Kibana (what was the ELK Stack is now the Elastic Stack).',
    alt: 'elk'
  }
];

var userData = [];

var str = {};

class Tools extends React.Component {
  constructor() {
    super();
    this.state = {
      open: false,
      userName: '',
      Password: '',
      ipAddress: '',
      ipError: '',
      arr: [],
      arrindex: [],
      stepIndex: 0,
      selectedTools: false,
      allTools: true,
      displayIP: true,
      selectTool: [],
      toolsConfigured: [],
      nameArr: [
        "Docker",
        "Nginx",
        "Gitlab",
        "SonarQube",
        "Jenkins",
        "Nagios",
        "Selenium",
        "Gerrit",
        "Ansible",
        "ELK"
      ],
      url: [
        "10.209.42.16:docker",
        "10.209.42.16:nginx",
        "10.209.42.16:",
        "10.209.42.16:",
        "10.209.42.16:",
        "10.209.42.16:",
        "10.209.42.16:",
        "10.209.42.16:",
        "10.209.42.16:",
        "10.209.42.16:"
      ],
      imageStatus: false
    };
    this.handleOpen = this.handleOpen.bind(this);
    this.provisionToolsSet = this.provisionToolsSet.bind(this);
  };
  componentWillMount() {
    let ip = '52.191.116.119';
    let userName = 'rig';
    let context = this;
    $.ajax({
      url: '/users/getUserData',
      method: 'POST',
      data: {
        ip: ip,
        userName: userName
      },
      success: function(data) {
        console.log("data", data);
        if (data != 'empty') {
          context.setState({toolsConfigured: data.toolsConfigured});
        }
      }.bind(this),
      error: function(err) {
        console.log("err", err);
      }.bind(this)
    })
  }

  handleOpen(e, i) {
    this.state.toolsConfigured.includes(e)
      ? this.state.toolsConfigured.splice(this.state.toolsConfigured.indexOf(e), 1)
      : this.state.toolsConfigured.push(e);
    this.state.selectTool.includes(e.name)
      ? this.state.selectTool.splice(this.state.selectTool.indexOf(e.name), 1)
      : this.state.selectTool.push(e.name);
    var toolsConfigured1 = this.state.toolsConfigured;
    this.setState({toolsConfigured: toolsConfigured1, imageStatus: true})
  }

  handleClose() {
    this.setState({open: false});
  };
  getUserName(e) {
    console.log("eeeeee", e.target.value);
    this.setState({userName: e.target.value})
    console.log("user", this.state.userName);
  };
  getPassword(e) {
    console.log("ssss", e.target.value);
    this.setState({Password: e.target.value})
  }

  saveData() {
    console.log("saved data");
    var obj = {
      username: this.state.userName,
      password: this.state.Password
    }
    console.log(obj);
    userData.push(obj);
    console.log(userData, "data");
  }
  provisionToolsSet() {
    if(this.props.alertStateAtTools) {

    } else {
        this.props.provisionSystem(this.state.toolsConfigured);
    }
  }
  render() {
    let warningMsg = '';

    if(this.props.alertStateAtTools) {
        warningMsg = (<div className="alert alert-info">
  <strong>Alert!</strong> We cannot connect to your server. But no worries! You can still use the power of RIG. We will provide you a git URL using which you can provision your tools.
</div>);
    }
    const actions = [
      <FlatButton label="Skip" primary={true} onClick={this.handleClose}/>,
      <FlatButton label="Submit" primary={true} onClick={this.saveData.bind(this)}/>
    ];
    const cus_style = {
      backgroundColor: "green"
    }

    let grids = tilesData.map((tile, i) => {
      return <Grid.Column key={i}>

        {/* <Card style={this.state.selectTool.includes(tile.name)
            ? {
              boxShadow: "2px 2px 9px 2px #00897B",
              margin: '10% 0'
            }
            : {
              margin: '10% 0'
            }} onClick={this.handleOpen.bind(this, tile, i)}>
          {
            this.state.selectTool.includes(tile.name)
              ? <Image fluid="fluid" label={{
                    color: 'teal',
                    corner: 'right',
                    icon: 'save'
                  }} src={tile.img} style={{
                    width: '100%',
                    height: '100px'
                  }}/>
              : <Image src={tile.img} alt={tile.alt} style={{
                    width: '100%',
                    height: '100px'
                  }}/>
          }
        </Card> */}
        <div onClick={this.handleOpen.bind(this, tile, i)}>
            <div className="card card-01 cardDesign">
              {
                this.state.selectTool.includes(tile.name) ?
                            <span className="glyphicon glyphicon-ok clickCursor" style={{zIndex: 1,color:"white", float: "right", marginRight:"15px", marginTop:"10px", background:"rgb(51, 122, 183)", borderRadius:"50%",padding:"5px"}}></span>:
                          <span className="glyphicon glyphicon-ok clickCursor" style={{zIndex: 1,color:"white", float: "right", marginRight:"15px", marginTop:"10px", background:"rgb(51, 122, 183)", borderRadius:"50%",padding:"5px",visibility:"hidden"}}></span> }
              <div className="clickCursor">
                      <div className="profile-box">
                          <img className="card-img-top" src={tile.img} alt={tile.name}/>
                      </div>
                      </div>
                      <div className="card-body text-center">
                        <div className="clickCursor">
                        <h4 className="card-title nameDesign">{tile.name}</h4>
                      </div>
                        {/* <p className="card-text">{this.props.cardContent}</p> */}
                      </div>
                      {/* <button onClick={this.restartJavaApp} className="btn btn-primary" style={{width:"30px",height:"30px",borderRadius:"50%",textAlign:"center"}}> */}

                      {/* </button> */}
            </div>
            </div>
      </Grid.Column>
    })

    return (<MuiThemeProvider muiTheme={muiTheme}>
      <div className="main-container" style={{
          marginLeft: '10%'
        }}>
        {warningMsg}
        {
          this.state.allTools == true
            ? <div className="Tools_wrapper">

                <Grid columns={4} relaxed="relaxed" className="grid-columns">
                  {grids}
                </Grid>
                {
                  this.state.selectTool.length
                    ? <div className="continueBtn">
                        <Button onClick={this.provisionToolsSet.bind(this)} style={{ backgroundColor: 'rgb(51, 122, 183)', color: 'white', marginLeft:"10%",fontSize:"20px" }}>Provision</Button>
                      </div>
                    : null
                }
              </div>
            : null
        }

        {
          this.state.selectedTools == true
            ? <div style={{
                  width: '80%',
                  margin: 'auto',
                  margin: '5%'
                }}>
                {
                  this.state.nameArr.map((item, i) => {
                    if (this.state.arrindex.includes(i)) {
                      if (i % 5 == 0) {
                        return <span>
                          <div><br/><br/></div><Popup trigger={<Icon name = "check circle" color = 'green' size = 'big' />} content={this.state.url[i]} basic="basic"/>
                          <b>
                            <span>
                              <RaisedButton style={btnStyle}>{item}</RaisedButton>
                            </span>
                          </b>
                          {
                            i == 0 || i % 5 != 4
                              ? <span>
                                  <figure className="circleDot" style={{
                                      backgroundColor: "#00897B"
                                    }}/><figure className="circleDot" style={{
                                    backgroundColor: "#00897B"
                                  }}/><figure className="circleDot" style={{
                                    backgroundColor: "#00897B"
                                  }}/><figure className="circleDot" style={{
                                    backgroundColor: "#00897B"
                                  }}/><figure className="circleDot" style={{
                                    backgroundColor: "#00897B"
                                  }}/>
                                </span>
                              : null
                          }
                        </span>
                      } else {
                        return <span><Popup trigger={<Icon name = "check circle" color = 'green' size = 'big' />} content={this.state.url[i]} basic="basic"/>
                          <b>
                            <span>
                              <RaisedButton style={btnStyle}>{item}</RaisedButton>
                            </span>
                          </b>
                          {
                            i == 0 || i % 5 != 4
                              ? <span>
                                  <figure className="circleDot" style={{
                                      backgroundColor: "#00897B"
                                    }}/><figure className="circleDot" style={{
                                    backgroundColor: "#00897B"
                                  }}/><figure className="circleDot" style={{
                                    backgroundColor: "  #00897B"
                                  }}/><figure className="circleDot" style={{
                                    backgroundColor: "#00897B"
                                  }}/><figure className="circleDot" style={{
                                    backgroundColor: "#00897B"
                                  }}/>
                                </span>
                              : null
                          }
                        </span>
                      }

                    } else {
                      if (i % 5 == 0) {
                        return <span>
                          <div><br/><br/><br/></div><Icon name="check circle" color='grey' size='big'/>
                          <b>
                            <span>
                              <RaisedButton style={btnStyle}>{item}</RaisedButton>
                            </span>
                          </b>
                          {
                            i == 0 || i % 5 != 4
                              ? <span>
                                  <figure className="circleDot" style={{
                                      backgroundColor: "#00897B"
                                    }}/><figure className="circleDot" style={{
                                    backgroundColor: "#00897B"
                                  }}/><figure className="circleDot" style={{
                                    backgroundColor: "#00897B"
                                  }}/><figure className="circleDot" style={{
                                    backgroundColor: "#00897B"
                                  }}/><figure className="circleDot" style={{
                                    backgroundColor: "#00897B"
                                  }}/>
                                </span>
                              : null
                          }
                        </span>
                      } else {
                        return <span><Icon name="check circle" color='grey' size='big'/>
                          <b>
                            <span>
                              <RaisedButton style={btnStyle}>{item}</RaisedButton>
                            </span>
                          </b>

                          {
                            i == 0 || i % 5 != 4
                              ? <span>
                                  <figure className="circleDot" style={{
                                      backgroundColor: "#00897B"
                                    }}/><figure className="circleDot" style={{
                                    backgroundColor: "#00897B"
                                  }}/><figure className="circleDot" style={{
                                    backgroundColor: "  #00897B"
                                  }}/><figure className="circleDot" style={{
                                    backgroundColor: "#00897B"
                                  }}/><figure className="circleDot" style={{
                                    backgroundColor: "#00897B"
                                  }}/>
                                </span>
                              : null
                          }
                        </span>
                      }
                    }
                  })
                }
              </div>
            : null
        }

      </div>
    </MuiThemeProvider>);
  }
}

Tools.contextTypes = {
  Paper: React.PropTypes.object
};

module.exports = Tools;
