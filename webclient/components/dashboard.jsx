import React from 'react';
import {
  Grid,
  Card,
  Dimmer,
  Header,
  Icon,
  Segment,
  Button,
  Image,
  Table,
  Menu,
  Input,
  Form,
  Dropdown,
  Popup,
  Accordion
} from 'semantic-ui-react';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import TextField from 'material-ui/TextField';
import { Draggable, Droppable } from 'react-drag-and-drop';
let {hashHistory} = require('react-router');
// const {ToastContainer} = ReactToastr;
// const ToastMessageFactory = React.createFactory(ReactToastr.ToastMessage.animation);

const muiTheme = getMuiTheme({

});

class Dashboard extends React.Component {
  constructor() {
    super();
    this.state = {
      stack:[{img : '../assets/images/mern-stack.png'},{img : '../assets/images/mean.jpeg'},{img : '../assets/images/python.jpg'},{img : '../assets/images/java.jpg'}],
      activateDimmer:false,
      selectedStack:'',
      sourceControlUrl:'',
      sourceControlUserName:'',
      sourceControlRepoName:'',
      activeIndex: 0,
      activateAcc:0,
      jobName:'',
      selectedItems: []
    };
    this.onDrop = this.onDrop.bind(this);
  }
  accordionClick(e, index){
      let activeIndex = this.state.activeIndex;
      const newIndex = activeIndex === index ? -1 : index
      this.setState({ activeIndex: newIndex })
    }
selectStack(e){
  this.setState({activateDimmer:true,selectedStack:e})
}
deActivateDimmer(){
    this.setState({activateDimmer:false})
}
getSourceUrl(e){
  this.setState({sourceControlUrl:e.target.value});
}
getUserName(e){
    this.setState({sourceControlUserName:e.target.value});
}
getPassword(e){
    this.setState({sourceControlPassword:e.target.value});
}
getRepoName(e){
  this.setState({sourceControlRepoName:e.target.value});
}
getJobName(e){
  this.setState({jobName:e.target.value});
}
selectTools(e){
  console.log("qqqqqqqqqq",e);
}
createJob() {
  let context = this;
    let data1 = "hudson.scm.NullSCM";
    let name = this.state.jobName;
    let content = '';
    let workflows = "workflow-job@2.12.2";
    let urlss = "org.jenkinsci.plugins.workflow.cps.CpsFlowDefinition";
    let plugs = "workflow-cps@2.40";
    // if (this.state.selected == 'pipeline') {
      content = "<flow-definition plugin='" + workflows + "'><description/><keepDependencies>false</keepDependencies><properties><org.jenkinsci.plugins.workflow.job.properties.PipelineTriggersJobProperty><triggers/></org.jenkinsci.plugins.workflow.job.properties.PipelineTriggersJobProperty></properties><definition class='" + urlss + "' plugin='" + plugs + "'><script/><sandbox>true</sandbox></definition><triggers/><disabled>false</disabled></flow-definition>";
    // } else {
    //   content = "<?xml version='1.0' encoding='UTF-8'?><project><keepDependencies>false</keepDependencies><properties/><scm class='" + data1 + "'/><canRoam>false</canRoam><disabled>false</disabled><blockBuildWhenDownstreamBuilding>false</blockBuildWhenDownstreamBuilding><blockBuildWhenUpstreamBuilding>false</blockBuildWhenUpstreamBuilding><triggers/><concurrentBuild>false</concurrentBuild><builders/><publishers/><buildWrappers/></project>";
    // }
    $.ajax({
      url: 'http://digitalrig.eastus.cloudapp.azure.com:8081/createItem?name=' + name,
      method: 'POST',
      contentType: "application/xml",
      dataType: "xml",
      beforeSend: function(request) {
        request.setRequestHeader("Authorization", "Basic " + btoa('wiprorig:digitalrig@123'));
      },
      data: content,
      success: function(data) {
        console.log("success", data);
        context.configureStack.bind(this);
      }.bind(this),
      error: function(err) {
        console.log("err", err);
      }.bind(this)
    })
  }
configureStack(){
  let selectedStack = this.state.selectedStack;
  let sourceControlUrl = this.state.sourceControlUrl;
  let sourceControlUserName = this.state.sourceControlUserName;
  let sourceControlPassword = this.state.sourceControlPassword;
  let sourceControlRepoName = this.state.sourceControlRepoName;
  let jobName = this.state.jobName;
  // console.log(selectedStack,sourceControlUrl,sourceControlUserName,sourceControlPassword,sourceControlRepoName);
  this.setState({activateDimmer:false});
    // let data1 = "hudson.scm.NullSCM";
                                // let content = '';
                                // let workflows = "workflow-job@2.12.2";
                                // let urlss = "org.jenkinsci.plugins.workflow.cps.CpsFlowDefinition";
                                // let plugs = "workflow-cps@2.40";
                                // if( this.state.selected == 'pipeline') {
                                                // content = "<flow-definition plugin='"+workflows+"'><description/><keepDependencies>false</keepDependencies><properties><org.jenkinsci.plugins.workflow.job.properties.PipelineTriggersJobProperty><triggers/></org.jenkinsci.plugins.workflow.job.properties.PipelineTriggersJobProperty></properties><definition class='"+urlss+"' plugin='"+plugs+"'><script/><sandbox>true</sandbox></definition><triggers/><disabled>false</disabled></flow-definition>";
                                // }
                                // else{
                                                // content = "<?xml version='1.0' encoding='UTF-8'?><project><keepDependencies>false</keepDependencies><properties/><scm class='"+data1+"'/><canRoam>false</canRoam><disabled>false</disabled><blockBuildWhenDownstreamBuilding>false</blockBuildWhenDownstreamBuilding><blockBuildWhenUpstreamBuilding>false</blockBuildWhenUpstreamBuilding><triggers/><concurrentBuild>false</concurrentBuild><builders/><publishers/><buildWrappers/></project>";
                                // }
                                // $.ajax({
                                //            url:'http://10.142.207.15:8100/createItem?name=' +jobName,
                                //            method:'POST',
                                //            contentType: "application/xml",
                                //            dataType: "xml",
                                //            beforeSend: function(request) {
                                //       request.setRequestHeader("Authorization", "Basic " + btoa('adapt:indian@123'));
                                //   },
                                //            data: content,
                                //            success:function(data){
                                //                            console.log("success",data);
                                //            }.bind(this),
                                //            error:function(err){
                                //                            console.log("err",err);
                                //            }.bind(this)
                                // })

    let d1 = "workflow-job@2.12.2";
    let d2 = "blueocean-rest-impl@1.2.1";
    let d3 = "io.jenkins.blueocean.service.embedded.BlueOceanUrlObjectImpl";
    let d4 = "gitlab-plugin@1.4.8";
    let d5 = "org.jenkinsci.plugins.workflow.cps.CpsFlowDefinition";
    let d6 = "workflow-cps@2.40";

    $.ajax({
                                                url:'http://digitalrig.eastus.cloudapp.azure.com:8081/job/'+jobName+'/config.xml',
                                                method:'POST',
                                                beforeSend: function(request) {
                                      request.setRequestHeader("Authorization", "Basic " + btoa('wiprorig:digitalrig@123'));
                                  },
      // <flow-definition plugin='"+workflows+"'><description/><keepDependencies>false</keepDependencies><properties><org.jenkinsci.plugins.workflow.job.properties.PipelineTriggersJobProperty><triggers/></org.jenkinsci.plugins.workflow.job.properties.PipelineTriggersJobProperty></properties><definition class='"+urlss+"' plugin='"+plugs+"'><script/><sandbox>true</sandbox></definition><triggers/><disabled>false</disabled></flow-definition>
      data:"<flow-definition plugin='"+d1+"'><description/><keepDependencies>false</keepDependencies><properties><org.jenkinsci.plugins.workflow.job.properties.PipelineTriggersJobProperty><triggers/></org.jenkinsci.plugins.workflow.job.properties.PipelineTriggersJobProperty></properties><definition class='"+urlss+"' plugin='"+plugs+"'><script/><sandbox>true</sandbox></definition><triggers/><disabled>false</disabled><actions><io.jenkins.blueocean.service.embedded.BlueOceanUrlAction plugin='"+d2+"'><blueOceanUrlObject class='"+d3+"'><mappedUrl>blue/organizations/jenkins/Maxapp</mappedUrl></blueOceanUrlObject></io.jenkins.blueocean.service.embedded.BlueOceanUrlAction> </actions> <description/> <keepDependencies>false</keepDependencies> <properties> <com.dabsquared.gitlabjenkins.connection.GitLabConnectionProperty plugin='"+d4+"'> <gitLabConnection>Gitlab_Jenkins</gitLabConnection> </com.dabsquared.gitlabjenkins.connection.GitLabConnectionProperty> <org.jenkinsci.plugins.workflow.job.properties.PipelineTriggersJobProperty> <triggers> <hudson.triggers.SCMTrigger> <spec>* * * * *</spec> <ignorePostCommitHooks>false</ignorePostCommitHooks> </hudson.triggers.SCMTrigger> </triggers> </org.jenkinsci.plugins.workflow.job.properties.PipelineTriggersJobProperty> </properties> <definition class='"+d5+"' plugin='"+d6+"'> <script> node { stage('checkin') {  git credentialsId: '1063a20b-765d-4348-bfb0-28675809e8ec', url: 'http://38fc218a1af0/wiprorig/testing.git' } "+"\n"+" stage('Code Quality') { "+"\n"+" stage('test') { parallel( CodeCoverage: { echo 'Code Coverage passed !!' }, UnitTest: { echo 'unit test passed !!' } ) } } "+"\n"+" stage('deploy'){ echo 'deployment passed !!' } "+"\n"+" stage('Functional Testing') { "+"\n"+" stage('test') { parallel( TDM: { echo 'Test Data Management passed !!' }, TEM: { echo 'Test Environment Management passed !!' }, FT: { echo 'functional test' } ) } } "+"\n"+" stage('Release'){ echo 'Released' } } </script> <sandbox>true</sandbox> </definition> <triggers/> <disabled>false</disabled> </flow-definition>",

                                                success:function(data){
                                                                console.log("success",data);
                                                }.bind(this),
                                                error:function(err){
                                                                console.log("err",err);
                                                }.bind(this)
                                })
  // $.ajax({
  //   url: '/users/repoToSourceControl',
  //   type: 'POST',
  //   data:{
  //     selectedStack,
  //     sourceControlUrl,
  //     sourceControlPassword,
  //     sourceControlRepoName
  //   },
  // traditional:true,
  //   success: function(result) {
  //     console.log("success...........",result);
  //     if(result == 'success'){
  //       $.ajax({
  //         url: '/users/codeToSourceControl',
  //         type: 'GET',
  //         success: function(result) {
  //           console.log("success...........",result);
  //         },
  //         error: function(err) {
  //           console.log("err");
  //         }
  //       });
  //     }
  //   },
  //   error: function(err) {
  //     console.log("err");
  //   }
  // });

}

  render() {
     const { activeIndex } = this.state


    return (
      <MuiThemeProvider muiTheme={muiTheme}>
        <div style={{marginLeft:'10%', marginTop:'3%'}}>
        <FloatingActionButton style={{
          position: 'absolute',
          bottom:'-5%',
          right:'2%',
          cursor:'pointer',
          zIndex: '1'
        }}
        onClick={
          this.props.handleAdd
        }
        >
          <ContentAdd

          />
        </FloatingActionButton>
        <Grid divided='vertically'>
          <Grid.Row columns={3}>
            <Grid.Column >
              <Card >
                  <Image src='../assets/images/rig1.jpg'
                  fluid
                  label={{ color: 'black', content: 'RIG-1', ribbon: true }}
                  style={{height:'142px'}}
                  />
                  <Card.Content>
                    <Card.Header style={{ textAlign:'center'}}>
                      RIG-1
                    </Card.Header>

                  </Card.Content>
                </Card>
            </Grid.Column>
            <Grid.Column >
              <Card >
                  <Image src='../assets/images/rig1.jpg'
                  fluid
                  label={{ color: 'black', content: 'RIG-2', ribbon: true }}
                  style={{height:'142px'}}
                  />
                  <Card.Content>
                    <Card.Header style={{ textAlign:'center'}}>
                      RIG-2
                    </Card.Header>

                  </Card.Content>
                </Card>
            </Grid.Column>
            <Grid.Column >
              <Card>
                  <Image src='../assets/images/rig1.jpg'
                  fluid
                  label={{ color: 'black', content: 'RIG-3', ribbon: true }}
                  style={{height:'142px'}}
                  />
                  <Card.Content>
                    <Card.Header style={{ textAlign:'center'}}>
                      RIG-3
                    </Card.Header>

                  </Card.Content>
                </Card>
            </Grid.Column>
            <Grid.Column style={{marginTop:'2%'}}>
              <Card>
                  <Image src='../assets/images/rig1.jpg'
                  fluid
                  label={{ color: 'black', content: 'RIG-4', ribbon: true }}
                  style={{height:'142px'}}
                  />
                  <Card.Content>
                    <Card.Header style={{ textAlign:'center'}}>
                      RIG-4
                    </Card.Header>

                  </Card.Content>
                </Card>
            </Grid.Column>
            <Grid.Column style={{marginTop:'2%'}}>
              <Card >
                  <Image src='../assets/images/rig1.jpg'
                  fluid
                  label={{ color: 'black', content: 'RIG-5', ribbon: true }}
                  style={{height:'142px'}}
                  />
                  <Card.Content>
                    <Card.Header style={{ textAlign:'center'}}>
                      RIG-5
                    </Card.Header>

                  </Card.Content>
                </Card>
            </Grid.Column>
          </Grid.Row>
        </Grid>
          </div>
      </MuiThemeProvider>
      )
}
onDrop(data) {
  let items = this.state.selectedItems.concat([data]);
  // items.filter((obj, index, arr) => {
  //   return arr.map(mapObj => mapObj['vc']).indexOf(obj['vc']) === index;
  // });
  this.setState({selectedItems: items});
  console.log(this.state.selectedItems);
}

}
  module.exports = Dashboard;
