import React from 'react';
import Paper from 'material-ui/Paper';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TextField from 'material-ui/TextField';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import { CardActions, CardMedia, CardHeader, CardTitle, CardText} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import { Grid, Segment, Divider } from 'semantic-ui-react';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import Toggle from 'material-ui/Toggle';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import { Card, Icon, Popup, Button } from 'semantic-ui-react';
import {
  Step,
  Stepper,
  StepButton,
  StepLabel
} from 'material-ui/Stepper';

const toggleStyle = {
  toggle: {
    marginTop: 0,
    marginLeft: 200,
  },
};

const popStyle = {
  borderRadius: 0,
  opacity: 0.8,
  padding: '2em',
}

const styles = {
  margin: 14,
  backgroundColor: "#000000ba !important",
};

const style = {
  height: 250,
  width: '80%',
  margin: 'auto',
  textAlign: 'center',
  marginTop: '3%'
};

const cbStyle = {
  margin: 12,
};

const btnStyle = {
  marginLeft: '0%',
  color: "black",
  borderStyle : "outset"
};

const muiTheme = getMuiTheme({

});

const Logged = (props) => (
  <IconMenu
    {...props}
    iconButtonElement={
      <IconButton><MoreVertIcon /></IconButton>
    }
    targetOrigin={{horizontal: 'right', vertical: 'top'}}
    anchorOrigin={{horizontal: 'right', vertical: 'top'}}
  >
    <MenuItem primaryText="Refresh" />
    <MenuItem primaryText="Help" />
    <MenuItem primaryText="Sign out" />
  </IconMenu>
);

Logged.muiName = 'IconMenu';

const tilesData = [
{
  img: '../assets/images/gitlab.png',
  text: 'Version Control system. Connecting issue management, code review, CI, CD, and monitoring into a single, easy-to-install application.',
  alt: 'GitLab'
},
{
  img: '../assets/images/sonarqube.jfif',
  text: 'Version Control system. Connecting issue management, code review, CI, CD, and monitoring into a single, easy-to-install application.',
  alt: 'SonarQube'
},
{
  img: '../assets/images/jenkins.png',
  text: 'Continuous Integration and Continuous Delivery. Jenkins can be used as a simple CI server or turned into the continuous delivery hub.',
  alt: 'Jenkins'
},
{
  img: '../assets/images/nagios.png',
  text: 'A powerful monitoring system. It provides enterprise-class Source IT monitoring, network monitoring, server and applications monitoring.',
  alt: 'Nagios'
},
{
  img: '../assets/images/selenium.png',
  text: 'Selenium Grid and Automated Testing which is suitable for web applications across different browsers and different platforms.',
  alt: 'Selenium'
},
{
  img: '../assets/images/gerrit.png',
  text: 'Code Review for Git. Gerrit provides web based code review and repository management for the Git version control system.',
  alt: 'Gerrit'
},
{
  img: '../assets/images/ansible.png',
  text: 'The simplest way to automate apps and IT infrastructure. Application Deployment + Configuration Management + Continuous Delivery.',
  alt: 'Ansible'
},
{
  img: '../assets/images/elk.png',
  text: 'ELK is a combination of the massively popular Elasticsearch, Logstash, and Kibana (what was the ELK Stack is now the Elastic Stack).',
  alt: 'ELK'
},
];

var userData = [];

var str = {};

class Pipeline extends React.Component {
  constructor() {
    super();
    this.state = {
      open:false,
        userName:'',
        Password:'',
        arr:[],
        arrindex:[],
        stepIndex:0,
        selectedTools:false,
        allTools:true,
        nameArr:["Docker","Nginx","Gitlab","SonarQube","Jenkins","Nagios","Selenium","Gerrit","Nexus","ELK"],
        url:[
              "https://www.docker.com/",
              "https://www.nginx.com/",
              "https://about.gitlab.com",
              "https://www.sonarqube.org/",
              "https://jenkins.io/",
              "https://www.nagios.org/",
              "http://www.seleniumhq.org/",
              "https://www.gerritcodereview.com/",
              "https://xebialabs.com/technology/nexus/",
              "https://www.elastic.co/webinars/introduction-elk-stack"
            ]

    };
    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
  };

    handleOpen(e,i) {

      if(this.state.arrindex.includes(i)){
        let index = this.state.arrindex.indexOf(i);
        let deSelectedValues = this.state.arrindex.splice(index, 1);
        this.setState(deSelectedValues)
      }
      else{
      var str = e.split('/')[3];
      var value = str.substring(0, str.length-4)
      // var names = "sh "+value+".sh";
      // var a = this.state.arr;
      // a.push(names);
      var arrindex1 = this.state.arrindex;
      arrindex1.push(i);
      // url.push()
      this.setState({arrindex:arrindex1})
      // this.setState({arr:a});

      // if(value == 'gitlab'){
      //   this.setState({open: 'git'});
      // }
      // else if(value == 'sonarqube'){
      //   this.setState({open: 'sonarqube'});
      // }
      // else if(value == 'nginx'){
      //   this.setState({open: 'nginx'});
      // }
    }
    }
    provisionSystem(){
        this.setState({allTools:false,selectedTools:true});
          console.log("....................",this.state.arrindex);
    }
    handleClose() {
        this.setState({open: false});
    };
    getUserName(e){
      console.log("eeeeee",e.target.value);
      this.setState({userName:e.target.value})
      console.log("user",this.state.userName);
    };
    getPassword(e){
      console.log("ssss",e.target.value);
      this.setState({Password:e.target.value})
    }


    saveData(){
      console.log("saved data");
      var obj={
        username:this.state.userName,
        password:this.state.Password
      }
      console.log(obj);
      userData.push(obj);
      console.log(userData, "data");
    }

  render() {
    const actions = [
      <FlatButton
        label="Skip"
        primary={true}
        onClick={this.handleClose}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        onClick={this.saveData.bind(this)}
      />,
    ];
     const cus_style = {
       backgroundColor:"green"
     }

    let grids = tilesData.map((tile, i) => {
          return <Grid.Column key={i}>
                    <Paper style={style} zDepth={5} className="raised-card"  onClick={this.handleOpen.bind(this,tile.img,i)}
                     style= {this.state.arrindex.includes(i) ? {boxShadow: "2px 2px 9px 2px green"} : null}>
                      <Popup
                      trigger={<img src={tile.img} alt={tile.alt} className="img"
                          />}
                          content={tile.text}
                          style={popStyle}
                          inverted
                        />
                    </Paper>
                  </Grid.Column>
              })

    return (
      <MuiThemeProvider muiTheme={muiTheme}>
      <div className="main-container" style={{marginLeft:'10%'}}>
      <div style={{width: '100%', marginLeft:'-6%' , marginTop:'8%'}}>
          {this.state.nameArr.map((item,i)=>{
           if(this.state.arrindex.includes(i)){
             if(i % 5 == 0){
               return <span key={i}><div><br/><br/></div><Popup
                 trigger={<Icon name="check circle" color='green' size='big'/>}
                 content={this.state.url[i]}
                 basic
               /><b><span>
               <Popup
                 trigger={
                          <RaisedButton style={btnStyle}
                          onClick={() => {
                            window.open(this.state.url[i]);
                          }}
                          >
                          {item}
                          </RaisedButton>
                        }
                 content={this.state.url[i]}
               />
               </span></b>
                             { i==0 || i%5 != 4 ?  <span>  <figure className="circleDot" style={{backgroundColor:"#00897B"}} /><figure className="circleDot" style={{backgroundColor:"#00897B"}} /><figure className="circleDot" style={{backgroundColor:"#00897B"}} /><figure className="circleDot" style={{backgroundColor:"#00897B"}} /><figure className="circleDot" style={{backgroundColor:"#00897B"}} /> </span> : null}
                             </span>
                           }else{
                             return   <span><Popup
                 trigger={<Icon name="check circle" color='green' size='big'/>}
                 content={this.state.url[i]}
                 basic
               /><b><span>
                 <Popup
                 trigger={
                          <RaisedButton style={btnStyle}
                          onClick={() => {
                            window.open(this.state.url[i]);
                          }}
                          >
                          {item}
                          </RaisedButton>
                        }
                   content={this.state.url[i]}
                 />
               </span></b>
               { i==0 || i%5 != 4 ?  <span> <figure className="circleDot" style={{backgroundColor:"#00897B"}} /><figure className="circleDot" style={{backgroundColor:"#00897B"}} /><figure className="circleDot" style={{backgroundColor:"  #00897B"}} /><figure className="circleDot" style={{backgroundColor:"#00897B"}} /><figure className="circleDot" style={{backgroundColor:"#00897B"}} />  </span> : null}
               </span>
             }
           }else{
             if(i % 5 == 0){
               return <span><div><br/><br/><br/></div><Icon name="check circle" color='grey' size='big'/><b><span>
                         <Popup
                         trigger={
                                  <RaisedButton style={btnStyle}
                                  onClick={() => {
                                    window.open(this.state.url[i]);
                                  }}
                                  >
                                  {item}
                                  </RaisedButton>
                                }
                           content={this.state.url[i]}
                         />
                      </span></b>
               {  i==0 || i%5 != 4 ?  <span>  <figure className="circleDot" style={{backgroundColor:"#00897B"}} /><figure className="circleDot" style={{backgroundColor:"#00897B"}} /><figure className="circleDot" style={{backgroundColor:"#00897B"}} /><figure className="circleDot" style={{backgroundColor:"#00897B"}} /><figure className="circleDot" style={{backgroundColor:"#00897B"}} />  </span> : null}
               </span>
             }else{
             return   <span><Icon name="check circle" color='grey' size='big'/><b><span>
                         <Popup
                         trigger={
                                  <RaisedButton style={btnStyle}
                                  onClick={() => {
                                    window.open(this.state.url[i]);
                                  }}
                                  >
                                  {item}
                                  </RaisedButton>
                                }
                           content={this.state.url[i]}
                         />
                      </span></b>

             {  i==0 || i%5 != 4 ?  <span>  <figure className="circleDot" style={{backgroundColor:"#00897B"}} /><figure className="circleDot" style={{backgroundColor:"#00897B"}} /><figure className="circleDot" style={{backgroundColor:"  #00897B"}} /><figure className="circleDot" style={{backgroundColor:"#00897B"}} /><figure className="circleDot" style={{backgroundColor:"#00897B"}} />  </span> : null}
             </span>
           }
           }
          })}
          </div>
          <br/>
          <br/>
          <Button style={{
            textAlign: 'center',
            marginLeft:'80%',
            marginTop: '5%',
            backgroundColor: '#00897B',
            color: 'white'
          }}
          onClick={this.props.handlePipeline}
          >Continue</Button>
        </div>
      </MuiThemeProvider>
    );
  }
}
Pipeline.contextTypes = {
  Paper: React.PropTypes.object
};

module.exports = Pipeline;
