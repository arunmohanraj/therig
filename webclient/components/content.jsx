import React from 'react';
import Login from './Login/Login.jsx';
import iplogin from './iplogin.jsx';
import Tools from './Tools.jsx';
import Main from './main.jsx';
import Dashboard from './dashboard.jsx';
import Stack from './stack.jsx';
import Pipeline from './pipeline.jsx';

export default class Content extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    switch (this.props.sidebarItemSelected) {
      case 'adminHome':
          return <Dashboard {...this.props}/>;
      case 'dashboard':
          return <Dashboard {...this.props}/>;
      case 'tools':
          return <Tools {...this.props}/>;
      case 'pipeline':
          return <Pipeline/>;
      case 'stack':
          return <Stack/>;
      case 'help':
          return <Dashboard/>;
    }
  }
}
