import React from 'react';
import {Accordion, Card, Label, Header} from 'semantic-ui-react';
import Paper from 'material-ui/Paper';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TextField from 'material-ui/TextField';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import {
  Step,
  Stepper,
  StepButton,
  StepLabel
} from 'material-ui/Stepper';
import FlatButton from 'material-ui/FlatButton';
import { hashHistory } from 'react-router';

const style = {
  height: 450,
  width: '80%',
  margin: 'auto',
  textAlign: 'center',
  marginTop: '5%'
};

const muiTheme = getMuiTheme({

});

const Logged = (props) => (
  <IconMenu
    {...props}
    iconButtonElement={
      <IconButton><MoreVertIcon /></IconButton>
    }
    targetOrigin={{horizontal: 'right', vertical: 'top'}}
    anchorOrigin={{horizontal: 'right', vertical: 'top'}}
  >
    <MenuItem primaryText="Refresh" />
    <MenuItem primaryText="Help" />
    <MenuItem primaryText="Sign out" />
  </IconMenu>
);

class statusBar extends React.Component {
  constructor() {
    super();
    this.state = {
      stepIndex: 0,
      stepIndex1: 4,
    };
  }

    handleNext() {
      const {stepIndex} = this.state;
      const {stepIndex1} = this.state;
      if (stepIndex < 2) {
        this.setState({stepIndex: stepIndex + 1});
      }
      if (stepIndex1 < 2) {
        this.setState({stepIndex1: stepIndex1 + 1});
      }
    };

    handlePrev() {
      const {stepIndex} = this.state;
      const {stepIndex1} = this.state;
      if (stepIndex > 0) {
        this.setState({stepIndex: stepIndex - 1});
      }
      if (stepIndex1 > 0) {
        this.setState({stepIndex1: stepIndex1 - 1});
      }
    };

    getStepContent(stepIndex) {
      switch (stepIndex) {
        case 0:
          return 'docker url';
        case 1:
          return 'nginx url';
        case 2:
          return 'gitlab url';
        case 3:
          return 'sonarqube url';
        case 4:
          return 'jenkins url';
        default:
          return 'Select the tool to be opened';
      }
    }

    getStepContent(stepIndex1) {
      switch (stepIndex1) {
        case 1:
          return 'nagios url';
        case 2:
          return 'selenium url';
        case 3:
          return 'gerrit url';
        case 4:
          return 'ansible url';
        case 5:
          return 'elk url';
        default:
          return 'Select the tool to be opened';
      }
    }

  render() {
    const {stepIndex} = this.state;
    const {stepIndex1} = this.state;
    const contentStyle = {margin: '5% 40%'};

    return (
      <MuiThemeProvider muiTheme={muiTheme}>
      <div className="main-container">
          <AppBar
            title="The Rig"
            className="navbar"
            iconClassNameLeft="<div></div>"
            iconElementRight={<Logged />}
          />
          <div className="status">
            <div style={{width: '100%', marginTop: "15%"}}>
              <Stepper linear={false} activeStep={stepIndex}>
                <Step>
                  <StepButton onClick={() => this.setState({stepIndex: 0})}>
                    Docker
                  </StepButton>
                </Step>
                <Step>
                  <StepButton onClick={() => this.setState({stepIndex: 1})}>
                    NGINX
                  </StepButton>
                </Step>
                <Step>
                  <StepButton onClick={() => this.setState({stepIndex: 2})}>
                    GitLab
                  </StepButton>
                </Step>
                <Step>
                  <StepButton onClick={() => this.setState({stepIndex: 3})}>
                    SonarQube
                  </StepButton>
                </Step>
                <Step>
                  <StepButton onClick={() => this.setState({stepIndex: 4})}>
                    Jenkins
                  </StepButton>
                </Step>
              </Stepper>
              <Stepper linear={false} activeStep={stepIndex1}>
                <Step>
                  <StepButton onClick={() => this.setState({stepIndex1: 1})}>
                    Nagios
                  </StepButton>
                </Step>
                <Step>
                  <StepButton onClick={() => this.setState({stepIndex: 6})}>
                    Selenium
                  </StepButton>
                </Step>
                <Step>
                  <StepButton onClick={() => this.setState({stepIndex: 7})}>
                    Gerrit
                  </StepButton>
                </Step>
                <Step>
                  <StepButton onClick={() => this.setState({stepIndex: 8})}>
                    Ansible
                  </StepButton>
                </Step>
                <Step>
                  <StepButton onClick={() => this.setState({stepIndex: 9})}>
                    ELK
                  </StepButton>
                </Step>
              </Stepper>
              <div style={{width: '100%', maxWidth: 700, margin: 'auto'}}>
              this.state
                <Stepper activeStep={stepIndex}>
                  <Step>
                    <StepLabel>Select campaign settings</StepLabel>
                  </Step>
                  <Step>
                    <StepLabel>Create an ad group</StepLabel>
                  </Step>
                  <Step>
                    <StepLabel>Create an ad</StepLabel>
                  </Step>
                </Stepper>

              </div>
              <div style={contentStyle}>

                <div style={{marginTop: 12}}>
                  <RaisedButton
                    label="Back"
                    // disabled={stepIndex === 0}
                    onClick={this.handlePrev.bind(this)}
                    style={{marginRight: 12}}
                  />
                  <RaisedButton
                    label="Next"
                    disabled={stepIndex === 0}
                    primary={true}
                    onClick={this.handleNext.bind(this)}
                  />
                </div>
              </div>
            </div>
          </div>
      </div>
      </MuiThemeProvider>
    );
  }
}

statusBar.contextTypes = {
    Paper: React.PropTypes.object
};


module.exports = statusBar;

      // {this.state.nameArr.map((item,i)=>{
      //
      //   <Stepper activeStep={this.state.stepIndex}>
      //     <Step>
      //       <StepLabel>{item}</StepLabel>
      //     </Step>
      //
      //   </Stepper>
      //
      // })}
