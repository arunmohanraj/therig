import React, { Component } from 'react';
import { hashHistory } from 'react-router';
import './styles.css';

export default class Login extends Component {
  constructor() {
    super();
    this.state = {
      username: '',
      password: '',
      error: '',
      validClick: false
    };
    this.login = this.login.bind(this);
    this.validate = this.validate.bind(this);
  }
  componentWillMount(){
    $.getScript("https://cdnjs.cloudflare.com/ajax/libs/particles.js/2.0.0/particles.min.js", function() {
      particlesJS('particles-js',
        {
          "particles": {
            "number": {
              "value": 80,
              "density": {
                "enable": true,
                "value_area": 800
              }
            },
            "color": {
              "value": "#ffffff"
            },
            "shape": {
              "type": "circle",
              "stroke": {
                "width": 0,
                "color": "#000000"
              },
              "polygon": {
                "nb_sides": 5
              },
              "image": {
                "width": 100,
                "height": 100
              }
            },
            "opacity": {
              "value": 0.5,
              "random": false,
              "anim": {
                "enable": false,
                "speed": 1,
                "opacity_min": 0.1,
                "sync": false
              }
            },
            "size": {
              "value": 5,
              "random": true,
              "anim": {
                "enable": false,
                "speed": 40,
                "size_min": 0.1,
                "sync": false
              }
            },
            "line_linked": {
              "enable": true,
              "distance": 150,
              "color": "#ffffff",
              "opacity": 0.4,
              "width": 1
            },
            "move": {
              "enable": true,
              "speed": 6,
              "direction": "none",
              "random": false,
              "straight": false,
              "out_mode": "out",
              "attract": {
                "enable": false,
                "rotateX": 600,
                "rotateY": 1200
              }
            }
          },
          "interactivity": {
            "detect_on": "canvas",
            "events": {
              "onhover": {
                "enable": true,
                "mode": "repulse"
              },
              "onclick": {
                "enable": true,
                "mode": "push"
              },
              "resize": true
            },
            "modes": {
              "grab": {
                "distance": 400,
                "line_linked": {
                  "opacity": 1
                }
              },
              "bubble": {
                "distance": 400,
                "size": 40,
                "duration": 2,
                "opacity": 8,
                "speed": 3
              },
              "repulse": {
                "distance": 200
              },
              "push": {
                "particles_nb": 4
              },
              "remove": {
                "particles_nb": 2
              }
            }
          },
          "retina_detect": true,
          "config_demo": {
            "hide_card": false,
            "background_color": "#b61924",
            "background_image": "",
            "background_position": "50% 50%",
            "background_repeat": "no-repeat",
            "background_size": "cover"
          }
        }
      );
    });
  }
  login() {
    let self = this;
    let { username, password } = this.state;
    if(this.validate()) {
      $.ajax({
          url: '/auth/login',
          type: 'POST',
          data: { username: username, password: password },
          success: function(response) {
            hashHistory.push("/main");
          },
          error: function(err) {
            console.log('err: ', err);
            self.setState({
              validClick: false,
              error: 'Invalid login! Try again.'
            });
          }
      });
    }
  }
  validate() {
    let { username, password } = this.state;
    if(username.trim().length === 0) {
      this.setState({ error: 'Username cannot be empty' });
      return false;
    } else if(password.trim().length === 0) {
      this.setState({ error: 'Password cannot be empty' });
      return false;
    } else {
      this.setState({ validClick: true });
      return true;
    }
  }
  render() {
    return (
      <div>
        <div className="container">
          <div id="login-box">
            <div className="logo">
              <img src="../../assets/images/logo.png" className="img img-responsive img-circle center-block"/>
              <h1 className="logo-caption"><span className="tweak">The Digital RIG</span></h1>
            </div>
            <div className="controls">
              <input type="text" placeholder="Username" className="form-control"
                onChange={(e) => this.setState({ username: e.target.value, error: "", validClick: false })}
              />
              <input type="password" placeholder="Password" className="form-control"
                onChange={(e) => this.setState({ password: e.target.value, error: "", validClick: false })}
              />
              <button className="btn btn-default btn-block btn-custom"
                disabled={this.state.validClick}
                onClick={this.login} >
                {
                  this.state.validClick ?
                  <i className="fa fa-spinner fa-spin"></i> :
                  'Login'
                }
              </button>
              <br/>
              <div className={ this.state.error.length ? "error-text" : "" }>
                { this.state.error }
              </div>
            </div>
          </div>
        </div>
        <div id="particles-js"></div>
      </div>
    );
  }
}
