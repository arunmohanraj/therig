import React from 'react';
import {Accordion, Card, Label, Header, Icon, Dimmer, Button} from 'semantic-ui-react';
import Paper from 'material-ui/Paper';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TextField from 'material-ui/TextField';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import { hashHistory } from 'react-router';
const ReactToastr = require('react-toastr');
const {ToastContainer} = ReactToastr;
const ToastMessageFactory = React.createFactory(ReactToastr.ToastMessage.animation);

const style = {
  height: 423,
  width: '100%',
  textAlign: 'center',
  marginTop: '20%'
};

const styles = {
  margin: 14,
  backgroundColor: "#000000ba !important",
  marginTop: 50,
};

const muiTheme = getMuiTheme({

});

const Logged = (props) => (
  <IconMenu
    {...props}
    iconButtonElement={
      <IconButton><MoreVertIcon /></IconButton>
    }
    targetOrigin={{horizontal: 'right', vertical: 'top'}}
    anchorOrigin={{horizontal: 'right', vertical: 'top'}}
  >
    <MenuItem primaryText="Refresh" />
    <MenuItem primaryText="Help" />
    <MenuItem primaryText="Sign out" />
  </IconMenu>
);

Logged.muiName = 'IconMenu';

class Login extends React.Component {
  constructor() {
    super();
    this.state = {
      username: '',
      password: '',
      usernameError: '',
      passwordError: '',
      currentUser: 'none',
      status:'false'
    };
    this.onUsernameChange = this.onUsernameChange.bind(this);
    this.onPasswordChange = this.onPasswordChange.bind(this);
    this.onLogin = this.onLogin.bind(this);
    this.successAlert = this.successAlert.bind(this);
    this.emptyDataAlert = this.emptyDataAlert.bind(this);
    this.errorAlert = this.errorAlert.bind(this);
  }

    onUsernameChange(event) {
          this.setState({
              usernameError: '',
              username: event.target.value
          });
      }

    onPasswordChange(event) {
            this.setState({
                passwordError: '',
                password: event.target.value
            });
        }

    emptyDataAlert() {
      this.refs.asd.info(
        'Please enter credentials',
        '', {
          timeOut: 3000,
          extendedTimeOut: 3000
        }
      );
    }

    successAlert() {
      this.refs.asd.success(
        'Successfully logged in ..',
        '', {
          timeOut: 3000,
          extendedTimeOut: 3000
        }
      );
    }

    errorAlert() {
      this.refs.asd.error(
        'Error in login',
        '', {
          timeOut: 3000,
          extendedTimeOut: 3000
        }
      );
    }

    onLogin() {
            localStorage.setItem('username',this.state.username);
             var username = localStorage.getItem('username');
        let self=this;
        if(this.state.username.length == 0 || this.state.password.length == 0) {
          alert("Please enter credentials");
          //this.emptyDataAlert();
        } else {
          this.setState({ status : true})
          let obj = this;
                     $.ajax({
                         url: '/auth/login',
                         type: 'POST',
                         data: {
                             username: obj.state.username,
                             password: obj.state.password
                         },
                         success: function(response) {
                             //obj.setState({authentication: true});
                               //obj.successAlert();
                               hashHistory.push("/main");
                         },
                         error: function(err) {
                             //obj.errorAlert();
                             alert("error in login");
                         }
                     })
        }
      }
  render() {
    return (
      <MuiThemeProvider muiTheme={muiTheme}>
      <div className="main-container login">
      <AppBar
        title="Digital Rig"
        className="navbar"
        iconClassNameLeft="<div></div>"
      />
          <div style={{marginLeft:'65%', width:'30%'}}>
          <Paper style={style} zDepth={1}>
            <div className="title">
              <h1 id="h1" style={{padding:'10%'}}>Welcome to Digital Rig!</h1>
            </div>
            <form>
            <div className="input-field" style={{marginTop:'2%'}}>
            <Icon name="user" style={{ marginRight: "4%"}}/>
            <TextField
              floatingLabelText="User Name"
              value={this.state.username}
              errorText={this.state.usernameError}
              onChange={this.onUsernameChange}
            />
            <br/>
            <Icon name="privacy" style={{ marginRight: "4%"}}/>
            <TextField
              floatingLabelText="Password"
              type="password"
              value={this.state.password}
              errorText={this.state.passwordError}
              onChange={this.onPasswordChange}
            />
            <br/>
            <br/>
            <br/>
            </div>
            <Button color="grey" onClick={this.onLogin.bind(this)}>Submit</Button>
            </form>
          </Paper>
          </div>
          {/*<AppBar
            className="footer"
            iconClassNameLeft="<div></div>"
          />*/}
          <ToastContainer ref='asd'
                  toastMessageFactory={ToastMessageFactory}
                  className='toast-top-left' style={{marginLeft:'28.5%'}}/>
      </div>
      </MuiThemeProvider>
    );
  }

  checkDocker() {
    //call backend api, and check response
    //if it is true redirect to tools page

    alert('Signing in...');
    hashHistory.push('/main');
  }
}

Login.contextTypes = {
    Paper: React.PropTypes.object
};


module.exports = Login;
