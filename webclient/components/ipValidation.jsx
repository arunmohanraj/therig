import React from 'react';
import {Accordion, Card, Label, Header, Dimmer, Icon, Button, Popup} from 'semantic-ui-react';
import Paper from 'material-ui/Paper';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TextField from 'material-ui/TextField';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import { hashHistory } from 'react-router';

const style = {
  height: 500,
  width: '60%',
  marginLeft: '15%',
  textAlign: 'center',
  marginTop: '10px'
};

const styles = {
  margin: 14,
  backgroundColor: "#000000ba !important",
  marginTop: 30,
};

const muiTheme = getMuiTheme({

});

const Logged = (props) => (
  <IconMenu
    {...props}
    iconButtonElement={
      <IconButton><MoreVertIcon /></IconButton>
    }
    targetOrigin={{horizontal: 'right', vertical: 'top'}}
    anchorOrigin={{horizontal: 'right', vertical: 'top'}}
  >
    <MenuItem primaryText="Refresh" />
    <MenuItem primaryText="Help" />
    <MenuItem primaryText="Sign out" />
  </IconMenu>
);

Logged.muiName = 'IconMenu';

class IpValidation extends React.Component {
  constructor() {
    super();
    this.state = {
      ip:'',
      ipError:'',
      rigname: '',
      projectname: '',
      userName: '',
      password: ''
    };
    this.onIpChange = this.onIpChange.bind(this);
    this.onLogin = this.onLogin.bind(this);
    this.storeCurrentIpAddress = this.storeCurrentIpAddress.bind(this);
  }

    onIpChange(event) {
          this.setState({
              ipError: '',
              ip: event.target.value
          });
      }
      onRigName(e){
        this.setState({rigname:e.target.value})
      }
      onProjectName(e){
        this.setState({projectname:e.target.value})
      }
      onUserName(e){
      this.setState({userName:e.target.value})
    }
    onPassword(e){
      this.setState({password:e.target.value})
    }

    onLogin() {

        let context = this;
        if(this.state.ip.length == 0 ) {
          alert("please enter IP");
        }else {
          let ip = context.state.ip;
          let userName = context.state.userName;
          let password = context.state.password;
          $.ajax({
            url: '/users/validateIpAddress',
            method: 'POST',
            data: {
              ip : ip,
              userName : userName,
              password : password
            },
            success: function(data) {
              console.log("success", data);
              if(data == "success") {
                context.storeCurrentIpAddress(true);
              } else if(data.code == 'ENOTFOUND' || data.level == 'authentication'){
                alert('ip address invalid / check credentials');
              } else {
                  context.storeCurrentIpAddress(false);
                  context.props.showAlertInToolsPage();
              }
            }.bind(this),
            error: function(err) {
              console.log("err", err);
            }.bind(this)
          })
          this.setState({ status : true})
        }
      }
storeCurrentIpAddress(status){
    let ip = '52.168.160.132';
    let userName = 'rig';
    $.ajax({
      url: '/users/storeCurrentIpAddress',
      method: 'POST',
      data: {
        ip : ip,
        userName : userName,
        serverAccess: status
      },
      success: function(data) {
        console.log("success", data);
        this.props.validSuccess();
      }.bind(this),
      error: function(err) {
        console.log("err", err);
      }.bind(this)
    })
  }
  render() {
    const {
      ip,
      ipError,
      rigname,
      projectname,
      userName,
      password
    } = this.state;

    return (
      <MuiThemeProvider muiTheme={muiTheme}>
      <div className="main-container" style={{marginLeft:'10%'}}>
          <div className="login_wrapper">
            <Paper style={style} zDepth={3}>
              <div>
                <Icon name="server" style={{marginRight: '3%'}}/>
                <TextField
                  floatingLabelText="IP address"
                  value={this.state.ip}
                  errorText={this.state.ipError}
                  onChange={this.onIpChange}
                  style={{cursor:"pointer", marginTop:"3%"}}
                />
              </div>
              <br/>
              <div>
                <Icon name="tags" style={{marginRight: '3%'}}/>
                <TextField
                  floatingLabelText="Rig Name"
                  value={this.state.rigname}
                  errorText={this.state.ipError}
                  onChange={this.onRigName.bind(this)}
                  style={{cursor:"pointer", marginTop:"-2%"}}
                />
              </div>
              <br/>
              <div>
                <Icon name="desktop" style={{marginRight: '3%'}}/>
                <TextField
                  floatingLabelText="Project Name"
                  value={this.state.projectname}
                  errorText={this.state.ipError}
                  onChange={this.onProjectName.bind(this)}
                  style={{cursor:"pointer", marginTop:"-2%"}}
                />
              </div>
              <br/>
              <div>
                <Icon name="user" style={{marginRight: '3%'}}/>
                <TextField
                  floatingLabelText="User Name"
                  value={this.state.userName}
                  errorText={this.state.ipError}
                  onChange={this.onUserName.bind(this)}
                  style={{cursor:"pointer", marginTop:"-2%"}}
                />
              </div>
              <br/>
              <div>
                <Icon name="key" style={{marginRight: '3%'}}/>
                <TextField
                  floatingLabelText="Password"
                  value={this.state.password}
                  errorText={this.state.ipError}
                  onChange={this.onPassword.bind(this)}
                  style={{cursor:"pointer", marginTop:"-2%"}}
                />
              </div>
              <br/>
              <br/>

              {
                (ip && rigname && projectname && userName && password ) ?
                  <Button  onClick={this.onLogin.bind(this)} style={{ backgroundColor: 'rgb(51, 122, 183)', color: 'white' }}>Submit</Button> :
                  null
              }
            </Paper>
          </div>
      </div>
      </MuiThemeProvider>
    );
  }

  checkDocker() {
    //call backend api, and check response
    //if it is true redirect to tools page

    alert('Connecting...');
    hashHistory.push('/tools');
  }
}

IpValidation.contextTypes = {
    Paper: React.PropTypes.object
};


module.exports = IpValidation;
