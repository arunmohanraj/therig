import React from 'react';
import Paper from 'material-ui/Paper';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TextField from 'material-ui/TextField';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import { CardActions, CardMedia, CardHeader, CardTitle, CardText} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import { Grid, Segment, Divider } from 'semantic-ui-react';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import Toggle from 'material-ui/Toggle';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Drawer from 'material-ui/Drawer';
import { Card, Icon, Popup, Step, Tab, Sidebar, Button, Menu, Image, Header, Dimmer, Loader } from 'semantic-ui-react';
import List from 'material-ui/List/List';
import ListItem from 'material-ui/List/ListItem';
import Avatar from 'material-ui/Avatar';
import SwipeableViews from 'react-swipeable-views';
import Content from './content.jsx';
const {Link} = require('react-router');
import iplogin from './iplogin.jsx';
import Tools from './Tools.jsx';
import Main from './main.jsx';
import Dashboard from './dashboard.jsx';
import Dashboard2 from './dashboard2.jsx';
import IpValidation from './ipValidation.jsx';
import Stack from './stack.jsx';
import Pipeline from './pipeline.jsx';
const ReactToastr = require('react-toastr');
const {ToastContainer} = ReactToastr;
const ToastMessageFactory = React.createFactory(ReactToastr.ToastMessage.animation);


const toggleStyle = {
  toggle: {
    marginTop: 0,
    marginLeft: 200,
  },
};

const styles = {
  margin: 14,
  backgroundColor: "#000000ba !important",
};

const style = {
  height: 250,
  width: '80%',
  margin: 'auto',
  textAlign: 'center',
  marginTop: '3%'
};

const cbStyle = {
  margin: 12,
};

const btnStyle = {
  color: "grey",
};

const muiTheme = getMuiTheme({

});

const Logged = (props) => (
  <IconMenu
    {...props}
    iconButtonElement={
      <IconButton><MoreVertIcon /></IconButton>
    }
    targetOrigin={{horizontal: 'right', vertical: 'top'}}
    anchorOrigin={{horizontal: 'right', vertical: 'top'}}
  >
    <MenuItem primaryText="Refresh" />
    <MenuItem primaryText="Help" />
    <MenuItem primaryText="Sign out" />
  </IconMenu>
);

Logged.muiName = 'IconMenu';

class MainPage extends React.Component {
  constructor() {
    super();
    this.state = {
        stepperoneStatus:true,
        steppertwoStatus:false,
        stepperthreeStatus:false,
        stepperfourStatus:false,
        stepperfiveStatus:false,
        waitDimmer: false,
        provisionStatus: false,
        disableStepperOne: false,
        disableStepperTwo: false,
        disableStepperThree: true,
        disableStepperFour: true,
        disableStepperFive: true,
        dashboardChange: false,
	      toolsConfigured :[],
        alertStateAtTools: false
    };
    this.provisionSystem = this.provisionSystem.bind(this);
    this.handleIpCheck = this.handleIpCheck.bind(this);
    this.handlePipeline = this.handlePipeline.bind(this);
    this.validSuccess = this.validSuccess.bind(this);
    this.dashBoardRedirect = this.dashBoardRedirect.bind(this);
    this.handleAdd = this.handleAdd.bind(this);
    this.showAlertInToolsPage = this.showAlertInToolsPage.bind(this);
  };
  showAlertInToolsPage() {
    this.setState({alertStateAtTools:true})
  }
  provisionSystem(e) {
   console.log("Tool list from child: ",e);
   let context = this;
   let ip = '52.191.116.119';
   let serverIp = '52.168.160.132';
   let serverUserName = 'rig';
   let serverPassword = 'digitalrig@123';
   let toolsConfigured = e;
   context.setState({toolsConfigured :toolsConfigured });
   let userName = 'rig';

   let data1 = {
     userName:userName,
     ip:ip,
     toolsConfigured: toolsConfigured
   }
   $.ajax({
       url: '/users/createGitLabProject',
       type: 'POST',
       traditional:true,
       data:{
         ip:serverIp
       },
       success: function(result) {
         console.log("success in creating gitlab project. response received is: ",result);
         if(result == "success"){
         $.ajax({
           url: '/users/createConfiguration',
           type: 'POST',
           data:{
           toolsConfigured :JSON.stringify(toolsConfigured)},
           success: function(result) {
             console.log('success in creating configuration and the response is : ',result);
             if(result == "success"){
             $.ajax({
               url: '/users/pushToGitlab',
               type: 'POST',
               data:{
                 ip:serverIp
               },
               success: function(result) {
                 console.log('response after pushing to gitlab', result);
                 if(result == 'success'){
                   console.log('b4 ajax of update tools', result);
                   $.ajax({
                     url: '/users/updateTools',
                     type: 'POST',
                     data: {
                       userName:userName,
                       ip:ip,
                       toolsConfigured: JSON.stringify(toolsConfigured)
                     },
                     success: function(response) {
                       console.log("tools updated", response);
                       if(response == 'success') {
                         $.ajax({
                           url: '/users/cloneCodeToClientMachine',
                           type: 'POST',
                           data:{
                             ip:serverIp,
                             serverUserName:serverUserName,
                             serverPassword:serverPassword
                           },
                           success: function(response) {
                             console.log("cloned");

                           },
                           error: function(err) {
                             // alert("error in provisioning");
                           }
                         })
                       }
                     },
                     error: function(err) {
                       // alert("error in provisioning");
                       console.log("error while updating tools", err);
                     }
                   })
                 }
               },
               error: function(err) {
                 console.log("err");
               }
             });
           }
           },
           error: function(err) {
             console.log("err");
           }
         });
       }
   }
   })
   }


  handleIpCheck() {
    this.setState({
      stepperoneStatus:false,
      steppertwoStatus:false,
      stepperthreeStatus:true,
      stepperfourStatus:false,
      stepperfiveStatus:false
    })
  }

  handlePipeline() {
    this.setState({
      stepperoneStatus:false,
      steppertwoStatus:false,
      stepperthreeStatus:false,
      stepperfourStatus:false,
      stepperfiveStatus:true,
      disableStepperFive: false
    })
  }

  validSuccess() {
    this.setState({
      disableStepperThree: false,
      stepperoneStatus:false,
      steppertwoStatus:false,
      stepperthreeStatus: true,
      tepperfourStatus:false,
      stepperfiveStatus:false
    })
  }

  dashBoardRedirect() {
    this.setState({
      stepperoneStatus:true,
      steppertwoStatus:false,
      stepperthreeStatus:false,
      stepperfourStatus:false,
      stepperfiveStatus:false,
      dashboardChange: true
    })
  }

  handleAdd() {
    console.log("sdcszfdfgfdh");
    this.setState({
      stepperoneStatus:false,
      steppertwoStatus:true,
      stepperthreeStatus:false,
      stepperfourStatus:false,
      stepperfiveStatus:false,
      dashboardChange: true
    })
  }

  render() {
    return (
      <MuiThemeProvider muiTheme={muiTheme}>
      <div className="main-container">
          <div className="wrapper">
              <AppBar
                title="Digital Rig"
                className="navbar"
                iconClassNameLeft="<div></div>"
              />
              <Step.Group style = {{ width: "100%", height: "50px",  marginTop:"-3%", marginBottom:"-0.1%" }} >

              <Step active={this.state.stepperoneStatus} style = {{borderBottomLeftRadius:"0", width: "20%"}}
              disabled = {this.state.disableStepperOne}
              onClick={()=>{
                  this.setState({
                    stepperoneStatus:true,
                    steppertwoStatus:false,
                    stepperthreeStatus:false,
                    stepperfourStatus:false,
                    stepperfiveStatus:false
                  });
              }}
              >
                <Icon name='dashboard' style={{ fontSize: 'large' }}/>
                <Step.Content>
                  <Step.Title>Dashboard</Step.Title>
                </Step.Content>
              </Step>

                <Step active={this.state.steppertwoStatus} style = {{borderBottomLeftRadius:"0", width: "20%"}}
                disabled = {this.state.disableStepperTwo}
                onClick={()=>{
                    this.setState({
                      stepperoneStatus:false,
                      steppertwoStatus:true,
                      stepperthreeStatus:false,
                      stepperfourStatus:false,
                      stepperfiveStatus:false
                    });
                }}
                >
                  <Icon name='checkmark box' style={{ fontSize: 'large' }}/>
                  <Step.Content>
                    <Step.Title>IP Validation</Step.Title>
                  </Step.Content>
                </Step>

                <Step active = {this.state.stepperthreeStatus} style = {{borderBottomLeftRadius:"0", width: "20%"}}
                disabled = {this.state.disableStepperThree}
                onClick={()=>{
                    this.setState({
                      stepperoneStatus:false,
                      steppertwoStatus:false,
                      stepperthreeStatus:true,
                      stepperfourStatus:false,
                      stepperfiveStatus:false
                    });
                }}
                >
                  <Icon name='grid layout' style={{ fontSize: 'large' }}/>
                  <Step.Content>
                    <Step.Title>Tools</Step.Title>
                  </Step.Content>
                </Step>

                <Step active = {this.state.stepperfourStatus} style = {{borderBottomLeftRadius:"0", width: "20%"}}
                disabled = {this.state.disableStepperFour}
                onClick={()=>{
                    this.setState({
                      stepperoneStatus:false,
                      steppertwoStatus:false,
                      stepperthreeStatus:false,
                      stepperfourStatus:true,
                      stepperfiveStatus:false
                    });
                }}
                >
                  <Icon name='linkify' style={{ fontSize: 'large' }}/>
                  <Step.Content>
                    <Step.Title>Pipeline</Step.Title>
                  </Step.Content>
                </Step>

                <Step active = {this.state.stepperfiveStatus} style = {{borderBottomLeftRadius:"0", width: "20%"}}
                disabled = {this.state.disableStepperFive}
                onClick={()=>{
                    this.setState({
                      stepperoneStatus:false,
                      steppertwoStatus:false,
                      stepperthreeStatus:false,
                      stepperfourStatus:false,
                      stepperfiveStatus:true
                    });
                }}
                >
                  <Icon name='tasks' style={{ fontSize: 'large' }}/>
                  <Step.Content>
                    <Step.Title>Stack</Step.Title>
                  </Step.Content>
                </Step>
              </Step.Group>
                <Segment attached style = {{width: "100%",backgroundColor:"transparent",height:"470px", border: "none"}}>
                  {this.state.stepperoneStatus ?
                    <div style = {{ width:"100%", textAlign: "center" }}>
                          {
                            this.state.dashboardChange ?
                            <Dashboard2 /> :
                            <Dashboard handleAdd={this.handleAdd} />
                          }
                    </div> :
                    this.state.steppertwoStatus ?
                    <div style = {{ width:"100%", textAlign: "center" }}>
                          <IpValidation handleIpCheck={this.handleIpCheck} validSuccess={this.validSuccess} showAlertInToolsPage={this.showAlertInToolsPage}/>
                    </div> :
                    this.state.stepperthreeStatus ?
                    <div>
                      <Tools provisionSystem={this.provisionSystem.bind(this)} alertStateAtTools={this.state.alertStateAtTools}/>
                      <div style={{ textAlign: "center" }}>
                      </div>
                    </div> :
                    this.state.stepperfourStatus ?
                    <div style={{ textAlign: "center" }}>
                        <Pipeline handlePipeline={this.handlePipeline} />
                    </div> :
                    this.state.stepperfiveStatus ?
                    <div>
                        <Stack dashBoardRedirect={this.dashBoardRedirect}/>
                    </div> :
                    null
                  }

                    <Dimmer
                      active={this.state.waitDimmer}
                      page
                    >
                    { this.state.provisionStatus ?
                        <div>
                          <h3>
                            Your tools have been successfully provisioned.
                          </h3>
                          <Button inverted color="blue"
                          onClick={()=>{
                              this.setState({
                                stepperoneStatus:false,
                                steppertwoStatus:false,
                                stepperthreeStatus:false,
                                stepperfourStatus:true,
                                stepperfiveStatus:false,
                                waitDimmer: false
                              });
                          }}
                          >
                            View Tools
                          </Button>
                        </div> :
                        <Loader size='massive'>
                          Please be patient while we are providing your tools.
                        </Loader>
                      }
                    </Dimmer>

              </Segment>

              <ToastContainer ref='asd'
                  toastMessageFactory={ToastMessageFactory}
                  className='toast-top-left' style={{marginLeft:'28.5%'}}/>
            </div>
            </div>
      </MuiThemeProvider>
    );
  }


}

Main.contextTypes = {
    Paper: React.PropTypes.object
};


module.exports = MainPage;
