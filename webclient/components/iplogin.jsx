import React from 'react';
import {Accordion, Card, Label, Header, Dimmer, Icon, Button, Popup} from 'semantic-ui-react';
import Paper from 'material-ui/Paper';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TextField from 'material-ui/TextField';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import { hashHistory } from 'react-router';

const style = {
  height: 450,
  width: '80%',
  margin: 'auto',
  textAlign: 'center',
  marginTop: '5%'
};

const styles = {
  backgroundColor: "#000000ba !important",
  marginTop: 50,
  marginLeft:'15%'
};

const styles1 = {
  backgroundColor: '#000000ba !important',
  marginTop: 50,
  width: '130% !important',
};

const muiTheme = getMuiTheme({

});

const Logged = (props) => (
  <IconMenu
    {...props}
    iconButtonElement={
      <IconButton><MoreVertIcon /></IconButton>
    }
    targetOrigin={{horizontal: 'right', vertical: 'top'}}
    anchorOrigin={{horizontal: 'right', vertical: 'top'}}
  >
    <MenuItem primaryText="Refresh" />
    <MenuItem primaryText="Help" />
    <MenuItem primaryText="Sign out" />
  </IconMenu>
);

Logged.muiName = 'IconMenu';

class iplogin extends React.Component {
  constructor() {
    super();
    this.state = {
      ip:'',
      ipError:'',
      messageDimmer: true
    };
    this.onIpChange = this.onIpChange.bind(this);
    this.onLogin = this.onLogin.bind(this);
  }

  componentWillMount() {
    this.setState ({
      messageDimmer : true
    })
  }

    onIpChange(event) {
          this.setState({
              ipError: '',
              ip: event.target.value
          });
      }


    onLogin() {
    let self=this;
    if(this.state.ip.length == 0 ) {
      alert("please enter IP");

    }else {
      this.setState({ status : true})
      hashHistory.push("/tools");
    }
  }
  render() {
    const { messageDimmer } = this.state;
    return (
      <MuiThemeProvider muiTheme={muiTheme}>
      <div className="main-container">
          <AppBar
            title="Digital Rig"
            className="navbar"
            iconClassNameLeft="<div></div>"
          />
          <div className="login_wrapper">
          <Paper style={style} zDepth={1}>
            <div className="title">
            </div>
            <div className="input-field">
            <TextField
              floatingLabelText="Enter IP address"
              value={this.state.ip}
              errorText={this.state.ipError}
              onChange={this.onIpChange}
              style={{marginTop:'10%'}}
            />
            <br/>
            <br/>
            </div>
            <RaisedButton label="Validate" primary={true} style={styles1} onClick={this.onLogin.bind(this)} />
            <RaisedButton label="Continue" primary={true} style={styles} onClick={this.onLogin.bind(this)} />
          </Paper>
          </div>
          <Dimmer active={messageDimmer} onClickOutside={this.handleHide}>
            <Header as='h2' icon inverted>
              <h2>Welcome to Digital RIG!</h2>
              <h3>You have not yet configured any tools.</h3>
            </Header>
            <br/><br/>
            <Popup
              position='right center'
              trigger={
                <Button inverted color="green"
                onClick = {() => {
                  this.setState({
                    messageDimmer: false
                  })
                }}
                >
                  Configure
                </Button>
                }
              content='Click here to start'
            />
          </Dimmer>
      </div>
      </MuiThemeProvider>
    );
  }

  checkDocker() {
    //call backend api, and check response
    //if it is true redirect to tools page

    alert('Connecting...');
    hashHistory.push('/tools');
  }
}

iplogin.contextTypes = {
    Paper: React.PropTypes.object
};


module.exports = iplogin;
