import React from 'react';
import ReactDOM from 'react-dom';
import {browserHistory, hashHistory, Route, Router} from 'react-router';
import Login from './components/Login/Login.jsx';
const Tools = require('./components/Tools.jsx');
const Status = require('./components/Status.jsx');
const iplogin = require('./components/iplogin.jsx');
const Main = require('./components/main.jsx');
const Stack = require('./components/stack.jsx');

ReactDOM.render(
  <Router history={hashHistory}>
      <Route path="/" component={Login}/>
      <Route path="/tools" component={Tools} />
      <Route path="/iplogin" component={iplogin} />
      <Route path="/status" component={Status} />
      <Route path="/main" component={Main} />
  </Router>, document.getElementById('app')
);
