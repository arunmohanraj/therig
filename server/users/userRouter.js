const router = require('express').Router();
// const passport = require('passport');
// let multer = require('multer');
// const Cookies = require ('universal-cookie');
// const cookies = new Cookies();

let userController = require('./userController.js');

router.post('/validateIpAddress',userController.validateIpAddress);
router.post('/storeCurrentIpAddress',userController.storeCurrentIpAddress);
router.post('/getUserData',userController.getUserData);
router.post('/updateTools',userController.updateTools);
router.post('/createGitLabProject',userController.createGitLabProject);
router.post('/createConfiguration',userController.createConfiguration);
router.post('/pushSampleCode',userController.pushSampleCode);
router.post('/pushToGitlab',userController.pushToGitlab);
router.post('/cloneCodeToClientMachine',userController.cloneCodeToClientMachine);


// router.post('/checkDocker',userController.checkDocker);
// router.post('/createConfiguration',userController.createConfiguration);
// router.get('/createGitLabProject',userController.createGitLabProject);
// router.get('/pushToGitlab',userController.pushToGitlab);
// router.post('/repoToSourceControl',userController.repoToSourceControl);
// router.get('/codeToSourceControl',userController.codeToSourceControl);
// router.get('/createJob',userController.createJob);
module.exports = router;
