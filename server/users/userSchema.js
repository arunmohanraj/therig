const mongoose = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');
mongoose.Promise = global.Promise;
const schema = new mongoose.Schema({
  userName: String,
    password: String,
    ip:[{
      ipAddress:String,
      toolsConfigured:[
        {toolsName:String}
      ]
    }]
});
autoIncrement.initialize(mongoose.connection);
let User = mongoose.model('user_data', schema);
module.exports = User;
