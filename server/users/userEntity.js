const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const schema = new mongoose.Schema({
  userName: String,
  password: String,
  ip: [
    {
      ipAddress: {
        type:String,
        unique: true
      },
      toolsConfigured:
      [
        {
          name:String,
          img: String,
          text: String,
          alt: String
        }
      ]
    }
  ]
});
let User = mongoose.model('user', schema);
module.exports = User;
