var shell = require('shelljs');
var exec = require('node-ssh-exec');
var fs = require('fs');
var path = require('path');
var gitlab = require('node-gitlab');
const User = require('./userEntity');
const execChild = require('child_process').exec;

let YAML = require('json2yaml');
let services = require("./services.json");

let userCtrl = {
  validateIpAddress: function(req, res) {
    let ip = req.body.ip;
    let userName = req.body.userName;
    let password = req.body.password;
    let config = {
        host: ip,
        username: userName,
        password: password
      }
    //   var ssh = new SSH({
    //       host: ip,
    //       user: userName,
    //       pass: password
    //     });
    //     ssh
    // .exec('ls', {
    //     out: console.log.bind(console)
    // })
    // .exec('git clone https://gitlab.com/keerthiii/52-168-160-132.git', {
    //     out: console.log.bind(console)
    // })
    // .exec('cd 52-168-160-132', {
    //     out: console.log.bind(console)
    // })
    // .exec('ls', {
    //     out: console.log.bind(console)
    // })
    // .start(
    //   {
    //     success: function() {
    //       console.log('success');
    //     },
    //     fail: function() {
    //       console.log('failure');
    //     }
    //   }
    // );
      command = `
        ls
      `;
    exec(config, command, function(error, response) {
      if (error) {
        console.log('ip validation',error);
        res.send(error);
      } else {
        console.log('---',response);
        res.send('success');
      }
    });
  },
  storeCurrentIpAddress: function(req, res) {
    let ip = req.body.ip;
    let userName = req.body.userName;
    User.find({userName: userName, "ip.ipAddress": ip}).then((docs) => {
      if (docs.length == 0) {
        User.findOneAndUpdate({
          userName: userName
        }, {
          '$push': {
            ip: {
              "ipAddress": ip,
              serverAccess: req.body.serverAccess
            }
          },
        }, {
          upsert: true
        }, function(err) {
          if (err) {
            res.send(err);
          } else {
            res.send("success");
          }
        });
      } else {
        res.send("success");
      }
    }, (err) => {
      res.send('Cant get the docs', err);
    });
  },
  getUserData: function(req, res) {
    let ip = req.body.ip;
    let userName = req.body.userName;

    User.find({userName: userName, "ip.ipAddress": ip}).then((docs) => {
      console.log(",,,,,", docs);
      if (docs.length != 0) {
        docs[0].ip.map((item) => {
          if (item.ipAddress == ip) {
            res.send(item);
          }
        })
      } else {
        res.send("empty");
      }
    }, (err) => {
      res.send('Cant get the docs', err);
    });

  },
  updateTools: function(req, res) {
    console.log('inside update tools');
    let ip = req.body.ip;
    let userName = req.body.userName;
    let toolsConfigured = req.body.toolsConfigured;
    let toolarr = [];

    toolarr.push(req.body.toolsConfigured);
    let tool = toolarr[0];
    User.find({userName: 'rig'}).then((docs) => {
      let arr = [];
      console.log('the docs: ', docs);
      docs[0].ip.map((item) => {
        if (item.ipAddress == ip) {
          item.toolsConfigured = toolsConfigured;
        }
        arr.push(item);
      })

      if (arr.length != 0) {
        User.findOneAndUpdate({
          userName: userName
        }, {
          $set: {
            ip: arr
          }
        }).then(function(docs1) {
          console.log('data',docs1);
          res.send('success');
        }, function(err1) {
          console.log('1. ',err1);
          res.send(err1);
        });
      }
    }, (err) => {
      console.log("2. ",err);
      res.send('Cant get the docs', err);
    });
  },
  createGitLabProject: function(req, res) {

    console.log("inside createGitLabProject: ", JSON.stringify(req.body));
    let ip = req.body.ip;
    var client = gitlab.create({api: 'https://gitlab.com/api/v3', privateToken: '7j4hu28JKwVBtfd9ku44'});
    let name = ip;
    client.projects.create({
      name:name,public:true
    }, function(err, response) {
      if (err) {
        console.log("Error in creating project in git. The error is : ", err);
        res.send('success');
      } else {
        console.log("success in creating project in git");
        res.send("success");
      }
    });
    // To replace particular portion

    // var someFile = "/home/rig/containers/jenkins.sh";
    //     fs.readFile(someFile, 'utf8', function (err,data) {
    //     if (err) {
    //       return console.log(err);
    //     }
    //     console.log("data",data);
    //     var result = data.replace(/'are'/g, 'replacement');
    //
    //     fs.writeFile(someFile, result, 'utf8', function (err,data) {
    //       console.log("datasssssssssss",someFile,result);
    //        if (err) return console.log(err);
    //
    //     });
    //   });
  },
  pushSampleCode: function(req, res) {
    let ip = req.body.ip;
    console.log("iiiiiiiiiiiiiii", ip);
    let ips = ip.replace(/\./g, '-');

    shell.cd('/home/rig/containers/');
    shell.exec('git init');
    shell.exec('git config --global user.name "root"');
    shell.exec('git config --global user.pass "murali123"');
    shell.exec('git add .');
    shell.exec('git commit -m "initial commit"');
    shell.exec('git remote add origin http://52.191.116.119:8080/root/' + ips + '.git');
    shell.exec('git push -u origin master');
    res.send("success");
  },
  // create configuration file
  createConfiguration: function(req, res) {
    console.log('inside createConfiguration');
    var toolsConfigured = JSON.parse(req.body.toolsConfigured);
    console.log("Tools configured : ", toolsConfigured);
    console.log(typeof toolsConfigured);
    let arr = [];
    toolsConfigured.map((item, index) => {
      arr[index] = item.name;

    })

    let getResolvedYAML = (yamlString) => {
      let finalYAML = ``;
      let lines = yamlString.split('\n');
      lines.map((line, index) => {
        if(index) {
          finalYAML += line.slice(2) + '\n';
        }
      });
      return finalYAML;
    };

    let dockerComposeJSON = {
  version: "3",
  services: {}
};
arr.map(tool => {
  let key = Object.keys(services[tool])[0];
  dockerComposeJSON.services[key] = services[tool][key];
});
let dockerComposeYAML = getResolvedYAML(YAML.stringify(dockerComposeJSON));
fs.writeFileSync("/home/arun/rig/config/docker-compose.yml", dockerComposeYAML);
fs.writeFileSync("/home/arun/rig/config/quickstart.sh", "sudo docker-compose up -d");

    res.send("success");
  },
  pushToGitlab: function(req, res) {
    console.log('inside pushToGitlab');
    let ip = req.body.ip;
    let ips = ip.replace(/\./g, '-')
    console.log('ip: ',ip);
    console.log('ips: ', ips);

// let commandForTrigger = 'sh repopush.sh '+ips;
let commandForTrigger = `
      cd /home/arun/rig/config
      cat docker-compose.yml
      git init
      git config remote.origin.url https://keerthiii:Keerthi_26@gitlab.com/keerthiii/${ips}.git
      git add .
      git commit -m "initial commit"
      git push origin master --force
      rm -rf .git
    `;
    execChild(commandForTrigger, (err, stdout, stderr) => {
            if (err) {
              console.error(err);
              return;
            }
            console.log('shell run: ',stdout);
    });
    // shell.cd('/home/rig/config');
    // shell.exec('git init');
    // shell.exec('git add .');
    // shell.exec('git commit -m "initial commit"');
    // shell.exec('git remote add origin http://52.191.116.119:8088/root/' + ips + '.git');
    // shell.exec('git config user.name "root"');
    // shell.exec('git config user.pass "murali123"');
    // shell.exec('git push -u origin master');
    res.send("success")

  },
  cloneCodeToClientMachine: function(req, res) {
    let ip = req.body.ip;
    let ips = ip.replace(/\./g, '-')
    let serverUserName = req.body.serverUserName;
    let serverPassword = req.body.serverPassword;
    console.log("inside clone code to gitlab");
    let config = {
        host: ip,
        username: serverUserName,
        password: serverPassword
      };
      console.log('config obj: ', config);
      command = `
          ls
          git clone https://gitlab.com/keerthiii/52-168-160-132.git
          cd 52-168-160-132
          sh quickstart.sh
        `;
    exec(config, command, function(error, response) {
      if (error) {
        console.log("error", error);
        res.send('failed');
      } else {
        console.log(",,,,,,,,,,", response);
        res.send('success');
      }
    });

  },
  // Check docker
  checkDocker: function(req, res) {
    let ip = req.body.ip;
    let userName = req.body.userName;
    let password = req.body.password;
    let config = {
        host: ip,
        username: userName,
        password: password
      },
      command = 'docker run --detach --publish 443:443 --publish 80:80 --publish 1220:22 --name gitlabDemo123 --restart always --volume /srv/gitlab/config:/etc/gitlab --volume /srv/gitlab/logs:/var/log/gitlab --volume /srv/gitlab/data:/var/opt/gitlab gitlab/gitlab-ce:latest';
    exec(config, command, function(error, response) {
      if (error) {
        console.log("errror", err);
        res.send('failed');
      } else {
        console.log("res", response);
        res.send(response);
      }
    });
  },
  // create sample repo to source Control
  repoToSourceControl: function(req, res) {
    // let selectedStack = req.body.selectedStack;
    // let sourceControlUrl = req.body.sourceControlUrl;
    // let sourceControlPassword = req.body.sourceControlPassword;
    // let sourceControlRepoName = req.body.sourceControlRepoName;
    // let url = sourceControlUrl+'/api/v3';
    //
    // var client = gitlab.create({
    //     api: url,
    //     privateToken: sourceControlPassword
    //   });
    //   let name=sourceControlRepoName;
    //   client.projects.create({name},function(err,response){
    //     if(err){
    //       console.log("err",err);
    //     }
    //     res.send("success");
    //   })
  },
  // push sample code to source control
  CodeToSourceControl: function(req, res) {
    // let selectedStack = req.body.selectedStack;
    // let sourceControlUrl = req.body.sourceControlUrl;
    // let sourceControlUserName = req.body.sourceControlUserName;
    // let sourceControlPassword = req.body.sourceControlPassword;
    // let sourceControlRepoName = req.body.sourceControlRepoName;
    // let url = sourceControlUrl+'/'+sourceControlUserName+'/'+sourceControlRepoName+'.git';
    // console.log("urlllllllll",url);
    // shell.echo('echo world');
    // shell.cd('/home/rig/containers/mern');
    // shell.exec('git init');
    // shell.exec('git add .');
    // shell.exec('git commit -m "initial commit"');
    //  shell.exec('git remote add origin http://10.142.207.15:8888/keerthiii/testingSample.git');
    // shell.exec('git push -u' +url+ 'master');
    res.send("success")
  },
  createJob: function(req, res) {
    // var jenkins = require('jenkins')({ baseUrl: 'http://adapt:indian%064%123@http://10.142.207.15:8100', crumbIssuer: true });
    // jenkins.job.create('example', xml, function(err) {
    //   if (err) throw err;
    // });
  }

}

module.exports = userCtrl;
