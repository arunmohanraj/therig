var LocalStrategy = require('passport-local').Strategy;
var bCrypt = require('bcrypt-nodejs');
var UserModel = require('../users/userEntity.js');

module.exports = function(passport) {
    passport.use('signup', new LocalStrategy({
            passReqToCallback: true
        },
        function(req, username, password, done) {
            UserModel.findOne({
                'userName': username
            }, function(err, user) {
                if (err) {
                    console.log('Error in SignUp: ' + err);
                    return done(err);
                }
                // user already exsist
                if (user) {
                  console.log(user,' exists');
                    return done(null, false);
                } else {
                    var newUser = new UserModel();
                    console.log('creating new user');
                    //set the user's credentials
                    newUser.userName = username;
                    newUser.password = createHash(password);


                    //saving into databases
                    newUser.save(function(err) {
                        if (err) {
                            console.log('Error in saving user in the database: ' + err);
                            throw err;
                        }
                        console.log('New user registered succesfully');
                        return done(null, newUser);
                    });
                }

            });
        }));
    // Generates hash using bCrypt

    var createHash = function(password) {
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);

    }
}
