/**
 * Created by Swathi on 15/01/2018.
 */
 const express = require('express');

const router = express.Router();

router.post('/createUser', require('./createUser'));
router.post('/getUser', require('./getUser'));
// router.post('/authentication',  require('./authentication'));
// router.get('/logout',  require('./logout'));

//The 404 Route (ALWAYS Keep this as the last route)
router.get('*', function(req, res){
    var err = new Error('Not Found');
    res.status(404);
    res.json(-1,`Requested API not found ${err}`);

});


module.exports = router;