const mongoose = require('mongoose');
var User = require('../users/userEntity');

var createUser = function(req, res, next) {
	console.log(req.body.ip, 'hello')
	try {
    let _newUser = new User({
      userName: req.body.userName,
      password: req.body.password,
			ip:[{
				ipAddress: req.body.ip.ipAddress,
				toolsConfigured:[
				  {toolsName: req.body.ip.toolsConfigured.toolsName}
				]
			}]
    });

    _newUser.save((err,user)=> {
    	console.log(err, user)
        if(err){
            console.log("ERROR IN SAVING USER",err);
          res.json(err);
        } else{
          res.json(user);
        }
    });
	} catch (err) {
        res.json(`Error on saving user ${err}`);
    }
}

module.exports = createUser;
