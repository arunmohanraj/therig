// const router = require('express').Router();
// const passport = require('passport');
//
// let userController = require('./userController.js');
//
// router.post('/login', passport.authenticate('local', {failureRedirect: '/'}), userController.login);
// router.post('/register',passport.authenticate('local',{failureRedirect:'/'}), userController.register);
//
// module.exports = router;
var express = require('express');
var router = express.Router();

var isAuthenticated = function (req, res, next) {
    // if user is authenticated in the session, call the next() to call the next request handler
    // Passport adds this method to request object. A middleware is allowed to add properties to
    // request and response objects
    if (req.isAuthenticated())
        return next();
    // if the user is not authenticated then redirect him to the login page
    res.redirect('/');
};
module.exports = function(passport) {
  console.log("route")
   /* signup action */
    router.post('/signup',function(req,res,next){
      console.log("---",req.body);
      passport.authenticate('signup',function(err,newUser,info){
        console.log(err,newUser,info);
        if(newUser){
           return res.status(200).json({status:'Successfully registered'});
        }
        else if(err){
          return res.status(500).json({status: 'Signup failed'});
        }
        else{
          return res.status(500).json({status:'User exists'});
        }
      })(req,res,next);
    });

    /*login action*/
    router.post('/login',function(req,res,next){
      passport.authenticate('login',function(err,user,info){
        console.log(user);
        if(user){
           return res.status(200).json({user: user});
        }
        else{
          return res.status(500).json(err);
        }
      })(req,res,next);
    });

   //  /*logout action*/
   //  router.get('/logOut',function(request, response) {
   //   console.log("in logout");
   //   request.session.destroy(function(req,res,err) {
   //     if(err) {
   //      console.log("status of error in logout" + err);
   //       response.status(500).json({status: 'error in logout'});
   //     } else {
   //       console.log("success in logout");
   //       response.status(200).json({status:'success'});
   //     }
   //   });
   // });
    //router.post('/updateProfile', profileController.updateProfile);
    return router;
}
