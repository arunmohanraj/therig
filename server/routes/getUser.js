const mongoose = require('mongoose');
var User = require('../users/userEntity');

var getUser = function(req, res, next) {
	try {
    User.findOne({ip: req.body.ip.ipAddress}, (err, user)=> {
          if(user) {
              res.json(user);
          } else {
              res.json(err);
          }
      })
	} catch (err) {
        res.json(`Error on saving user ${err}`);
    }
}

module.exports = getUser;
